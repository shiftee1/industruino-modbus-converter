# Modbus TCP Converter
Configures an Industruino D21G PLC to operate as a Modbus TCP -> RTU converter

This allows a Modbus RTU device to be connected to a Modbus TCP network.

Using the Ethernet library with the Industruino requires the following change:
 - change the SPI speed to 4MHz in the Ethernet2 library,
   /src/utility/w5500.cpp line #25, 
   as seen here: https://github.com/Industruino/Ethernet2/blob/master/src/utility/w5500.cpp
