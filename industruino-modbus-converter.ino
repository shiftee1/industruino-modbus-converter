/*
 * Copyright (C) 2018
 *     Mark O'Donovan <shiftee@posteo.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <UC1701.h>
#include <Ethernet2.h>
#include <SPI.h>
#include <Wire.h>

enum btn_t {
	BTN_NONE,
	BTN_UP,
	BTN_DOWN,
	BTN_ENTER,
	NUM_BTNS
};

enum baud_t {
	BAUD_1200,
	BAUD_2400,
	BAUD_4800,
	BAUD_9600,
	BAUD_14400,
	BAUD_19200,
	BAUD_28800,
	BAUD_38400,
	BAUD_57600,
	BAUD_115200,
	NUM_BAUDS
};

enum screen_t {
	SCREEN_INFO,
	SCREEN_DIAG,
	SCREEN_EDIT,
	NUM_SCREENS
};

struct ip_addr_t               //ip address struct
{
	uint8_t         oct0;          //octet 0
	uint8_t         oct1;          //octet 1
	uint8_t         oct2;          //octet 2
	uint8_t         oct3;          //octet 3
};

struct config_t                //configuration struct
{
	uint32_t        baud;          //rs485 baud rate
	uint32_t        port;          //tcp port
	uint32_t        pin;           //pin code
	ip_addr_t       ip_addr;       //device ip address
};

const unsigned RS485_DE       = 9;            //rs485 data enable
const unsigned RS485_RE_NEG   = 2;            //rs485 receiver enable
const unsigned LCD_NUM_ROWS   = 8;
const unsigned LCD_CHAR_WIDTH = 5+1;          //character width in pixels
const unsigned PKT_BUF_SIZE   = 6 + 253 + 2;  //header + modbus-pkt + crc
const unsigned STR_BUF_SIZE   = 22;
const unsigned EEPROM_ADDR    = 0x50;
const unsigned EEPROM_RTC     = 0x57;
const unsigned BACKLIGHT_PIN  = 26;
const unsigned EDITOR_TIMEOUT = 20;

//global variables
uint32_t  btn_press_tm = 0;    //last time a button was pressed
unsigned  screen_index = 0;    //current screen in menu
uint32_t  num_requests = 0;    //number of requests received
unsigned  cursor_row;          //cursor row in non-info menus
btn_t     last_btn_press;      //the last pressed button (or none)
byte      mac[6];              //ethernet mac address
bool      auth;                //is the user authenticated

uint8_t   buf[PKT_BUF_SIZE];

char      rx[STR_BUF_SIZE];
char      tx[STR_BUF_SIZE];
unsigned  rx_bytes;
unsigned  tx_bytes;

UC1701         lcd;            //lcd screen
config_t       config;         //configuration
EthernetServer server(0);      //ethernet server


/*
 * Read a byte from I2C EEPROM
 *     dev: device address
 *     reg: register address
 */
uint8_t eepromReadByte(uint8_t dev, uint8_t reg)
{
	delay(5);

	Wire.beginTransmission(dev);
	Wire.write(reg);
	Wire.endTransmission();

	Wire.requestFrom(dev, 1);

	return Wire.read();
}

/*
 * Write a byte to I2C EEPROM
 *     dev: device address
 *     reg: register address
 *     val: value to write
 */
int eepromWriteByte(uint8_t dev, uint8_t reg, uint8_t val)
{
	delay(5);

	Wire.beginTransmission(dev);

	Wire.write(reg);
	Wire.write(val);

	return Wire.endTransmission();
}

/*
 * Formats the data in src buffer as a hex string
 *
 * Returns the number of chars
 */
int bufToHex(char* dest, uint8_t* src, unsigned num_bytes)
{
	//null terminate if num_bytes == 0
	dest[0] = 0;

	int chars = 0;

	for(unsigned i=0; i<num_bytes; i++)
		chars += sprintf(dest+i*2, "%02X", src[i]);

	return chars;
}

/*
 * Formats the data in src buffer as a hex string
 * and limits the size to the buffer size
 *
 * Returns the number of chars
 */
int packetToString(char* dest, uint8_t* src, unsigned num_bytes)
{
	unsigned max_bytes = STR_BUF_SIZE / 2;

	if( num_bytes >= max_bytes )
		num_bytes = max_bytes - 1;

	return bufToHex(dest, src, num_bytes);
}

/*
 * Setup the RS485 port
 */
void setupRS485()
{
	pinMode(RS485_RE_NEG, OUTPUT);
	pinMode(RS485_DE,     OUTPUT);

	digitalWrite(RS485_RE_NEG, 0);
	digitalWrite(RS485_DE,     0);

	Serial.begin(config.baud);
}

/*
 * Send num_bytes from buf on the RS485 port
 *
 * Returns the number of bytes sent
 */
unsigned sendRS485(uint8_t* buf, unsigned num_bytes)
{
	digitalWrite(RS485_RE_NEG, 1);
	digitalWrite(RS485_DE,     1);

	unsigned bytes_sent = Serial.write(buf, num_bytes);

	Serial.flush();

	digitalWrite(RS485_RE_NEG, 0);
	digitalWrite(RS485_DE,     0);

	return bytes_sent;
}

/*
 * Returns true if the user has not pressed a button within the timeout
 *     timeout: timeout value in seconds
 */
bool isUserTimeout(unsigned timeout)
{
	return millis() - btn_press_tm > timeout * 1000;
}

/*
 * Setup the user-interface buttons
 */
void setupButtons()
{
	attachInterrupt(24, enterPressed, FALLING);
	attachInterrupt(25, upPressed,    FALLING);
	attachInterrupt(23, downPressed,  FALLING);
}

/*
 * Return the last pressed button
 */
btn_t getButtonPress()
{
	btn_t btn = last_btn_press;

	last_btn_press = BTN_NONE;

	return btn;
}

/*
 * Record a button press
 *
 * Limits button presses to one per 150ms
 */
void recordButtonPress(btn_t btn)
{
	//throttle button presses
	if( millis() - btn_press_tm > 150 )
	{
		last_btn_press = btn;
		btn_press_tm   = millis();
	}
}

/*
 * Record an Enter button press
 */
void enterPressed()
{
	recordButtonPress(BTN_ENTER);
}

/*
 * Record a Down button press
 */
void downPressed()
{
	recordButtonPress(BTN_DOWN);
}

/*
 * Record an Up button press
 */
void upPressed()
{
	recordButtonPress(BTN_UP);
}

/*
 * Converts our IP address object into the format used by the
 * Arduino Ethernet library
 */
IPAddress getIpAddr(ip_addr_t& addr)
{
	return IPAddress(addr.oct0, addr.oct1, addr.oct2, addr.oct3);
}

/*
 * Load the MAC address from the RTC EEPROM
 * We use the last 6 bytes of the 8 byte UI-64 MAC in the MCP79402 RTC,
 * registers 0xF0 to 0xF7
 */
void loadMAC()
{
	for(int i=0; i<6; i++)
		mac[i] = eepromReadByte(EEPROM_RTC, 0xF2 + i);
}

/*
 * Setup the Ethernet port
 */
void setupEthernet()
{
	loadMAC();

	IPAddress addr = getIpAddr(config.ip_addr);

	Ethernet.begin(mac, addr);

	server = EthernetServer(config.port);

	server.begin();
}

/*
 * Setup the LCD backlight
 */
void setupBacklight()
{
	pinMode(BACKLIGHT_PIN, OUTPUT);

	//enable backlight for config load message
	digitalWrite(BACKLIGHT_PIN, true);
}

/*
 * Manage the LCD backlight
 */
void manageBacklight()
{
	bool bl_enabled = true;

	if( isUserTimeout(12) )
		bl_enabled = false;

	digitalWrite(BACKLIGHT_PIN, bl_enabled);
}

/*
 * Return the integer baud value for the passed type
 */
uint32_t getRawBaud(baud_t baud_type)
{
	uint32_t val = 9600;

	switch( baud_type )
	{
	case BAUD_1200:
		val = 1200;
		break;
	case BAUD_2400:
		val = 2400;
		break;
	case BAUD_4800:
		val = 4800;
		break;
	case BAUD_9600:
		val = 9600;
		break;
	case BAUD_14400:
		val = 14400;
		break;
	case BAUD_19200:
		val = 19200;
		break;
	case BAUD_28800:
		val = 28800;
		break;
	case BAUD_38400:
		val = 38400;
		break;
	case BAUD_57600:
		val = 57600;
		break;
	case BAUD_115200:
		val = 115200;
		break;
	case NUM_BAUDS:
		break;
	}

	return val;
}

/*
 * Return the baud-type for the passed integer bad value
 *   e.g. 4800 would give BAUD_4800
 */
baud_t getBaudType(uint32_t raw_baud)
{
	baud_t baud_type = BAUD_9600;

	switch( raw_baud )
	{
	case 1200:
		baud_type = BAUD_1200;
		break;
	case 2400:
		baud_type = BAUD_2400;
		break;
	case 4800:
		baud_type = BAUD_4800;
		break;
	case 9600:
		baud_type = BAUD_9600;
		break;
	case 14400:
		baud_type = BAUD_14400;
		break;
	case 19200:
		baud_type = BAUD_19200;
		break;
	case 28800:
		baud_type = BAUD_28800;
		break;
	case 38400:
		baud_type = BAUD_38400;
		break;
	case 57600:
		baud_type = BAUD_57600;
		break;
	case 115200:
		baud_type = BAUD_115200;
		break;
	}

	return baud_type;
}

/*
 * Edit the passed baud-rate
 *     baud: e.g. 57600
 */
void editBaud(uint32_t& baud)
{
	lcd.clear();
	lcd.setCursor(10, 2);
	lcd.print("Baud");

	//get initial val
	int val = getBaudType(baud);
	int min = 0;
	int max = NUM_BAUDS - 1;

	uint32_t new_baud = baud;

	bool editing = true;

	while( editing )
	{
		switch( getButtonPress() )
		{
		case BTN_UP:
			if( val < max )
				val++;
			break;
		case BTN_DOWN:
			if( val > min )
				val--;
			break;
		case BTN_ENTER:
			editing = false;
			baud    = new_baud;
			break;
		case BTN_NONE:
		case NUM_BTNS:
			break;
		}

		new_baud = getRawBaud( static_cast<baud_t>(val) );

		lcd.setCursor(10, 4);
		lcd.print(">");
		lcd.print(new_baud);
		lcd.print("  ");

		delay(50);

		if( isUserTimeout(EDITOR_TIMEOUT) )
			break;
	}

	return;
}

/*
 * Return the number of chars in the passed number
 */
unsigned getChars(unsigned x)
{
	if( x < 10 )
		return 1;

	return 1 + getChars( x / 10 );
}

/*
 * Split the decimal characters of num into buf
 * e.g. 502 => [5,0,2]
 */
void numToChars(uint8_t* buf, unsigned num, unsigned num_chars)
{
	for(unsigned i=0; i<num_chars; i++)
	{
		buf[num_chars-i-1] = num % 10;
		num /= 10;
	}
}

/*
 * Assemble the decimal characters of buf into num
 * e.g. [5,0,2] => 502
 */
unsigned charsToNum(uint8_t* buf, unsigned num_chars)
{
	unsigned num = 0;

	for(unsigned i=0; i<num_chars; i++)
	{
		num *= 10;
		num += buf[i];
	}

	return num;
}

/*
 * Edit the passed value within the passed limits
 *
 * Returns 0 on success
 */
int editUnsigned(uint32_t& val, uint32_t min, uint32_t max, const char* msg)
{
	lcd.clear();
	lcd.setCursor(10, 2);
	lcd.print(msg);

	int status = -1;

	uint32_t new_val = val;

	bool editing = true;

	unsigned num_chars = getChars(max);
	uint8_t  chars[num_chars];
	numToChars(chars, new_val, num_chars);

	unsigned stage = 0;

	while( editing )
	{
		uint8_t* ptr = chars + stage;

		switch( getButtonPress() )
		{
		case BTN_UP:
			if( *ptr < 9 )
				(*ptr)++;
			break;
		case BTN_DOWN:
			if( *ptr > 0 )
				(*ptr)--;
			break;
		case BTN_ENTER:
			new_val = charsToNum(chars, num_chars);

			if( new_val > max )
				new_val = max;
			if( new_val < min )
				new_val = min;
			numToChars(chars, new_val, num_chars);

			//go through each character of the number
			stage++;
			if( stage >= num_chars )
			{
				editing = false;
				val     = new_val;
				status  = 0;
			}
			break;
		case BTN_NONE:
		case NUM_BTNS:
			break;
		}

		lcd.setCursor(10, 4);
		lcd.print(">");
		for(unsigned i=0; i<num_chars; i++)
			lcd.print(chars[i]);

		//mark the character being edited
		unsigned col = 10 + LCD_CHAR_WIDTH * (stage+1);
		lcd.setCursor(col, 5);
		lcd.print("^");

		delay(50);

		if( isUserTimeout(EDITOR_TIMEOUT) )
			break;
	}

	return status;
}

/*
 * Edit the passed IPv4 address
 */
void editAddrIPV4(ip_addr_t& addr)
{
	lcd.clear();

	ip_addr_t new_addr = addr;

	bool editing = true;

	unsigned stage = 0;

	while( editing )
	{
		uint8_t* val = nullptr;

		//go through four octets of ip address
		switch( stage )
		{
		case 0:
			val = &new_addr.oct0;
			break;
		case 1:
			val = &new_addr.oct1;
			break;
		case 2:
			val = &new_addr.oct2;
			break;
		case 3:
			val = &new_addr.oct3;
			break;
		}

		uint32_t edit_val = *val;

		int status = editUnsigned(edit_val, 0, 255, "IP");
		if( status != 0 )
			break;

		*val = edit_val;

		if( stage >= 3 )
		{
			editing = false;
			addr = new_addr;
		}

		stage++;
	}

	return;
}

/*
 * Calculates the CRC for num_bytes of buf
 */
uint16_t getCRC16(uint8_t* buf, unsigned num_bytes)
{
	uint16_t crc = 0xFFFF;

	for(unsigned i=0; i<num_bytes; i++)
	{
		crc ^= buf[i];

		for(int i=0; i<8; i++)
		{
			if( crc & 1 )
				crc = (crc >> 1) ^ 0xA001;
			else
				crc = (crc >> 1);
		}
	}

	return crc;
}

/*
 * Sets the default configuration
 */
void setDefaultConfig()
{
	config.pin        = 0;
	config.port       = 502;
	config.baud       = 57600;

	config.ip_addr = { 10,  130,  44, 245 };
}

/*
 * Save the system configuration to EEPROM
 */
void saveConfig()
{
	unsigned num_bytes = sizeof(config_t);

	uint8_t* p = (uint8_t*) &config;

	for(unsigned i=0; i<num_bytes; i++)
	{
		uint8_t val = *(p+i);

		eepromWriteByte(EEPROM_ADDR, i, val);
	}

	uint16_t crc = getCRC16(p, num_bytes);

	eepromWriteByte(EEPROM_ADDR, num_bytes++, lowByte(crc));
	eepromWriteByte(EEPROM_ADDR, num_bytes++, highByte(crc));
}

/*
 * Loads the system configuration from EEPROM
 *
 * Returns 0 on success
 */
int loadConfig()
{
	unsigned num_bytes = sizeof(config_t);

	uint8_t* p = (uint8_t*) &config;

	for(unsigned i=0; i<num_bytes; i++)
	{
		p[i] = eepromReadByte(EEPROM_ADDR, i);
	}

	uint16_t real_crc = getCRC16(p, num_bytes);

	buf[0] = eepromReadByte(EEPROM_ADDR, num_bytes++);
	buf[1] = eepromReadByte(EEPROM_ADDR, num_bytes++);

	uint16_t crc = buf[1] << 8 | buf[0];

	int status = 0;

	if( crc != real_crc )
	{
		setDefaultConfig();

		saveConfig();

		status = -1;
	}

	return status;
}

/*
 * Informs the user about the configuration load status
 *
 * This function blocks briefly if the config was loaded successfully,
 * otherwise it blocks until the user presses the enter key.
 */
void displayConfigLoadStatus(int status)
{
	if( status )
	{
		lcd.setCursor(0, 2);
		lcd.print("FAILED TO LOAD CONFIG");

		lcd.setCursor(20, 4);
		lcd.print("Press Enter");

		while( getButtonPress() != BTN_ENTER )
			delay(100);
	}
	else
	{
		lcd.setCursor(0, 1);
		lcd.print(" Modbus TCP Converter");
		lcd.setCursor(0, 3);
		lcd.print(" gitlab.com/shiftee1");
		lcd.setCursor(0, 5);
		lcd.print("    GPLv3 Software");
	}
}

/*
 * Setup the LCD screen
 */
void setupLCD()
{
	lcd.begin();

	cursor_row     = 0;
	last_btn_press = BTN_NONE;
}

/*
 * Setup the I2C bus
 */
void setupI2C()
{
	Wire.begin();
}

/*
 * Tries to authenticate a user
 *
 * Returns true if the user is authenticated
 */
bool checkAuth()
{
	if( config.pin == 0 )
		auth = true;

	if( ! auth )
	{
		uint32_t val = 5555;
		editUnsigned(val, 0, 9999, "Pin");

		auth = val == config.pin;
	}

	return auth;
}

/*
 * Show information about received modbus packets
 */
void showScreenDiagnostics()
{
	unsigned x;

	lcd.setCursor(0, 0);
	lcd.print("Packet info:");

	//print the request packet
	lcd.setCursor(0, 2);
	lcd.print("REQ: (");
	lcd.print(rx_bytes);
	lcd.print(" bytes)  ");
	lcd.setCursor(0, 3);
	lcd.print(" ");
	lcd.print(rx);
	x = rx_bytes*2;
	while( x++ < 20 )
		lcd.print(" ");

	//print the response packet
	lcd.setCursor(0, 4);
	lcd.print("RSP: (");
	lcd.print(tx_bytes);
	lcd.print(" bytes)  ");
	lcd.setCursor(0, 5);
	lcd.print(" ");
	lcd.print(tx);
	x = tx_bytes*2;
	while( x++ < 20 )
		lcd.print(" ");

	lcd.setCursor(0, 7);
	lcd.print("Requests: ");
	lcd.print(num_requests);

	switch( getButtonPress() )
	{
	case BTN_UP:
		if( screen_index > 0 )
			screen_index--;
		break;
	case BTN_DOWN:
		if( screen_index < NUM_SCREENS-1 )
			screen_index++;
		break;
	case BTN_ENTER:
	case BTN_NONE:
	case NUM_BTNS:
		if( isUserTimeout(EDITOR_TIMEOUT) )
			screen_index = SCREEN_INFO;
		break;
	}
}

/*
 * Print the passed ip address to the passed screen handle
 */
void printIp(UC1701& lcd, ip_addr_t& ip)
{
	lcd.print(ip.oct0);
	lcd.print(".");
	lcd.print(ip.oct1);
	lcd.print(".");
	lcd.print(ip.oct2);
	lcd.print(".");
	lcd.print(ip.oct3);
}

/*
 * Show basic device information
 *
 * To improve packet processing times we only update the screen if requested
 */
void showScreenInfo(bool screen_change)
{
	if( screen_change )
	{
		lcd.setCursor(0, 0);
		lcd.print("Modbus TCP -> RTU");

		lcd.setCursor(0, 2);
		lcd.print("Addr: ");
		printIp(lcd, config.ip_addr);

		lcd.setCursor(0, 3);
		lcd.print("Port: ");
		lcd.print(config.port);

		lcd.setCursor(0, 4);
		lcd.print("Baud: ");
		lcd.print(config.baud);
	}

	switch( getButtonPress() )
	{
	case BTN_UP:
		break;
	case BTN_DOWN:
		if( screen_index < NUM_SCREENS-1 )
			screen_index++;
		break;
	case BTN_ENTER:
	case BTN_NONE:
	case NUM_BTNS:
		break;
	}
}

/*
 * Show and edit system parameters
 */
void showScreenEdit()
{
	if( ! checkAuth() )
	{
		screen_index = SCREEN_INFO;
		return;
	}

	unsigned menu_row_min = 2;

	if( cursor_row < menu_row_min )
		cursor_row = menu_row_min;

	//draw cursor
	for(unsigned i=0; i<LCD_NUM_ROWS; i++)
	{
		char c = ' ';

		if( cursor_row == i )
			c = '>';

		lcd.setCursor(0, i);
		lcd.print(c);
	}

	unsigned row = 0;

	//columns are pixels
	unsigned start_col = LCD_CHAR_WIDTH;

	lcd.setCursor(start_col, row);
	lcd.print("Settings:");

	row++;
	row++;

	lcd.setCursor(start_col, row);
	lcd.print("Addr:");
	printIp(lcd, config.ip_addr);
	row++;

	lcd.setCursor(start_col, row);
	lcd.print("Port:");
	lcd.print(config.port);
	row++;

	lcd.setCursor(start_col, row);
	lcd.print("Baud:");
	lcd.print(config.baud);
	row++;

	lcd.setCursor(start_col, row);
	lcd.print("Pin: ");
	if( config.pin == 0 )
		lcd.print("0000");
	else
		lcd.print("****");

	unsigned menu_row_max = row;

	switch( getButtonPress() )
	{
	case BTN_UP:
		if( cursor_row > menu_row_min )
		{
			cursor_row--;
		}
		else
		{
			if( screen_index > 0 )
				screen_index--;
		}
		break;
	case BTN_DOWN:
		if( cursor_row < menu_row_max )
			cursor_row++;
		break;
	case BTN_ENTER:
		switch( cursor_row )
		{
		case 2:    //addr
			editAddrIPV4(config.ip_addr);

			setupEthernet();

			saveConfig();
			break;
		case 3:    //port
			editUnsigned(config.port, 0, 65535, "Port");

			setupEthernet();

			saveConfig();
			break;
		case 4:    //baud
			editBaud(config.baud);

			setupRS485();

			saveConfig();
			break;
		case 5:    //pin
			editUnsigned(config.pin, 0, 9999, "Pin");

			saveConfig();
			break;
		}
		break;
	case BTN_NONE:
	case NUM_BTNS:
		if( isUserTimeout(EDITOR_TIMEOUT) )
			screen_index = SCREEN_INFO;
		break;
	}
}

/*
 * Manage the user-interface
 */
void manageUI()
{
	//don't clear the info screen for 3 seconds on powerup
	if( btn_press_tm == 0 )
	{
		if( isUserTimeout(3) )
			btn_press_tm = 1;

		return;
	}

	//clear screen if changing menus
	static unsigned my_index = NUM_SCREENS;

	bool screen_change = my_index != screen_index;

	if( screen_change )
	{
		lcd.clear();
		my_index = screen_index;
		auth = false;
	}

	switch( screen_index )
	{
	case SCREEN_DIAG:
		showScreenDiagnostics();
		break;
	case SCREEN_EDIT:
		showScreenEdit();
		break;
	case SCREEN_INFO:
	case NUM_SCREENS:
	default:
		showScreenInfo(screen_change);
	}
}

/*
 * Return a character delay for the passed baud-rate
 * A slower baud-rate needs a longer delay
 */
unsigned getRxDelay(unsigned baud)
{
	unsigned delay_ms = 1;

	baud_t baud_type = getBaudType(baud);

	switch( baud_type )
	{
	case BAUD_1200:
	case BAUD_2400:
		delay_ms = 10;
		break;
	case BAUD_4800:
	case BAUD_9600:
	case BAUD_14400:
	case BAUD_19200:
	case BAUD_28800:
		delay_ms = 5;
		break;
	case BAUD_38400:
	case BAUD_57600:
	case BAUD_115200:
	case NUM_BAUDS:
		break;
	}

	return delay_ms;
}

/*
 * Read Modbus requests from the Ethernet port
 * and service them using the RS485 port
 */
void handleTcpToRtuConnections()
{
	EthernetClient client = server.available();

	if( client )
	{
		unsigned num_bytes = 0;

		//wait for data
		auto start = millis();
		while( millis() - start < 10 )
		{
			if( client.available() )
				break;
			delay(1);
		}

		//read data
		while( client.available() )
		{
			buf[num_bytes++] = client.read();

			if( num_bytes >= PKT_BUF_SIZE - 2 )
				break;

			if( ! client.available() )
				delay(1);
		}

		if( num_bytes > 6 )
		{
			num_bytes -= 6;

			uint8_t* pkt = buf+6;

			packetToString(rx, pkt, num_bytes);
			rx_bytes = num_bytes;
			num_requests++;

			//append crc
			uint16_t crc = getCRC16(pkt, num_bytes);
			pkt[num_bytes++] = lowByte(crc);
			pkt[num_bytes++] = highByte(crc);

			//flush the input buffer
			while( Serial.read() != -1 );

			num_bytes = sendRS485(pkt, num_bytes);

			//wait for response
			start = millis();
			while( millis() - start < 250 )
			{
				if( Serial.available() )
					break;
				delay(1);
			}

			unsigned delay_ms = getRxDelay(config.baud);

			//read data
			num_bytes = 0;
			while( Serial.available() )
			{
				pkt[num_bytes++] = Serial.read();

				if( num_bytes >= PKT_BUF_SIZE )
					break;

				if( ! Serial.available() )
					delay(delay_ms);
			}

			//remove crc
			if( num_bytes > 1 )
				num_bytes -= 2;

			packetToString(tx, pkt, num_bytes);
			tx_bytes = num_bytes;

			if( num_bytes > 0 )
			{
				//modify length field in header
				uint16_t len = num_bytes;
				buf[4] = highByte(len);
				buf[5] = lowByte(len);

				client.write(buf, num_bytes+6);
			}
		}
	}
}

/*
 * Setup the hardware and global variables
 */
void setup()
{
	setupI2C();

	setupLCD();

	setupButtons();

	setupBacklight();

	int status = loadConfig();

	displayConfigLoadStatus(status);

	setupRS485();

	setupEthernet();
}

/*
 * main loop - handle modbus conversions and run the UI
 */
void loop()
{
	handleTcpToRtuConnections();

	manageBacklight();

	manageUI();
}
